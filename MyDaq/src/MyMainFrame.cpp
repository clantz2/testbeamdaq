/**
 * @file MyMainFrame.cpp
 * Implementation for MyMainFrame class
 *
 * @brief
 *
 * @author Yakov Kulinich
 * @version 1.0
 */

#include "MyMainFrame.h"
#include "SharedData.h"
#include "MyDaq.h"

#include <stdio.h>

#include <TTree.h>
#include <TFile.h>
#include <TH1.h>
#include <TString.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TApplication.h>

#include <TGClient.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TRootEmbeddedCanvas.h>

#include <TTimer.h>

#include <iostream>
#include <unistd.h>
#include <limits.h>
#include <string>

MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h, MyDaq* daq) :
  TGMainFrame(p, w, h), m_daq(daq) {
  runNumber = 0;
  fileName = "";
  ElogH = NULL;

  // Create canvas widget
  m_fEcanvas = new TRootEmbeddedCanvas("Ecanvas", this, w/2, h);
  AddFrame(m_fEcanvas, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,
					 10,10,10,1));

  // timer handling.
  TCanvas *fCanvas = m_fEcanvas->GetCanvas();
  fCanvas->Divide(1,2);
  fCanvas->cd(1);
  m_daq->GetSharedData()->GetHists()["h_ADC_1_SUM"]->Draw();
  fCanvas->cd(2);
  m_daq->GetSharedData()->GetHists()["h_STDC_8_SUM"]->Draw();



  // Create a horizontal frame widget with buttons
  m_hframe = new TGHorizontalFrame(this,200,40);

  m_startB = new TGTextButton( m_hframe,"&Start");
  m_startB->Connect("Clicked()","MyMainFrame",this,"DoStart()");
  m_hframe->AddFrame(m_startB, new TGLayoutHints( kLHintsLeft, 5, 5, 3, 4 ) );

  m_stopB = new TGTextButton( m_hframe,"&Stop");
  m_stopB->Connect("Clicked()","MyMainFrame",this,"DoStop()");
  m_hframe->AddFrame(m_stopB, new TGLayoutHints( kLHintsRight, 5, 5, 3, 4 ) );

  m_fTextEntry = new TGTextEntry( m_hframe, new TGTextBuffer(40));
  m_hframe->AddFrame(m_fTextEntry, new TGLayoutHints( kLHintsLeft, 5, 5, 3, 4 ) );

  m_fTextEntry_num = new TGTextEntry( m_hframe, new TGTextBuffer(40));
  m_hframe->AddFrame(m_fTextEntry_num, new TGLayoutHints( kLHintsRight, 5, 5, 3, 4 ) );

  AddFrame(m_hframe, new TGLayoutHints( kLHintsCenterX, 2, 2, 2, 2 ) );

  // Set a name to the main frame
  SetWindowName("MUON DAQ");

  // Map all subwindows of main frame
  MapSubwindows();

  // Initialize the layout algorithm
  Resize(GetDefaultSize());

  // Map main frame
  MapWindow();

  // create a timer firing every 100 ms
  fTimer = new TTimer(this, 500);
}

MyMainFrame::~MyMainFrame() {
  // Clean up used widgets: frames, buttons, layout hints
  Cleanup();
}

/**
   Just tells the canvas to refresh
   whenever the timer fires
 */

Bool_t MyMainFrame::HandleTimer(TTimer *)
{
   // timer handling.
   TCanvas *fCanvas = m_fEcanvas->GetCanvas();

   fCanvas->cd();
   fCanvas->Update();
   fCanvas->Modified();
   fCanvas->Update();

   fTimer->Reset();

  return kTRUE;
}

void MyMainFrame::DoStart() {
  TString nm =  m_fTextEntry->GetBuffer()->GetString();
  TString temp =  m_fTextEntry_num->GetBuffer()->GetString();
  evLimit = std::stof((std::string)temp);
  //root
/*
  if( nm == "" )
    m_daq->m_fout_name = (TString)"output.root";
*/
      //counter file
      TFile file("../src/_counter_DO_NOT_REMOVE.root","UPDATE","counter");
      TTree *tree = (TTree*)file.Get("counter");
      Int_t count = 0;
      if (tree == NULL) {
        std::cerr<<"NO TREE EXIST"<<std::endl;
        TTree *tree = new TTree("counter","tree title");
        tree->Branch("count",&count,"count/I");

        count = 414; //first run of this testbeam is Run 414
        tree->Fill();
        tree->Write();
        delete tree;
      }
      else {
        tree->SetBranchAddress("count", &count);
        int nEntries = tree->GetEntries();

        tree->GetEntry(nEntries-1);
        count++;
        delete tree;
        file.Close();

        TFile file("../src/_counter_DO_NOT_REMOVE.root","RECREATE","counter");
        TTree *tree = new TTree("counter","tree title");
        tree->Branch("count",&count,"count/I");
        tree->Fill();
        tree->Write();
        delete tree;
      }
      file.Close();
    //std::cout << "NM: " << nm << std::endl;
  if( nm == "" ){
    SetFileName((std::string)Form("run_%08d-0000.root", (int)count));
    SetRunNumber((int)count);
    m_daq->m_fout_name = (TString)Form("run_%08d-0000.root", (int)count);
  }
  else{
    //std::string dummy;
    //dummy = (std::string)Form("%s", nm.Data()) + (std::string)Form("_%08d-0000.root", (int)count);
    //std::cout << "MTF: " << dummy << std::endl;
    SetFileName((std::string)Form("%s_%08d-0000.root", nm.Data(), (int)count));
    SetRunNumber((int)count);
    m_daq->m_fout_name = Form("%s_%08d-0000.root", nm.Data(), (int)count);
  }

  //elog
  std::cout<<"Set ElogHandler"<<std::endl;
  daq_set_eloghandler(GetHostName(), GetPort(), GetLogName());

  if(ElogH){
    ElogH->BegrunLog( GetRunNumber(),"MyMainFrame", GetFileName());
  }else{
    std::cerr<<"No ElogHandler"<<std::cout;
  }
  fTimer->Reset();
  fTimer->TurnOn();


  m_daq->m_start_flag = true;

}

void MyMainFrame::DoStop() {
  if (ElogH) {
    //fix this
    ElogH->EndrunLog( GetRunNumber(),"MyMainFrame", 0, 0, 0);
  }
  m_daq->m_stop_flag = true;


  fTimer->TurnOff();

  gApplication->Terminate(0);
}

int MyMainFrame::daq_set_eloghandler( const char *host, const int port, const char *logname)
{

  if ( ElogH) delete ElogH;
  ElogH = new ElogHandler (host, port, logname );

  setenv ( "DAQ_ELOGHOST", host , 1);
  char str [128];
  sprintf(str, "%d", port);
  setenv ( "DAQ_ELOGPORT", str , 1);
  setenv ( "DAQ_ELOGLOGBOOK", logname , 1);

  return 0;
}

char* MyMainFrame::GetHostName()
{
  char hostname[HOST_NAME_MAX];
  gethostname(hostname, HOST_NAME_MAX);
  //std::string str = hostname;
  return hostname;
}
const char* MyMainFrame::GetLogName()
{
  std::string output = "FermilabDAQ";
  return output.c_str();
}
